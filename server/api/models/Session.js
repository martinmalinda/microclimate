/**
* Session.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    date : { type: 'string' },

    place : { type: 'string' },

    weather: {type: 'string'},

    people: {type: 'integer'},

    lightMedian: {type: 'float'},
    tempMedian: {type: 'float'},
    rhMedian: {type: 'float'},
    co2Median: {type: 'float'},
    startTime: {type: 'string'},
  }
};


