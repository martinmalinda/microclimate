/**
* Value.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    session : { type: 'string' },

    time : { type: 'string' },

    type : { type: 'string' },

    value: {type: 'string'},
  }
};

