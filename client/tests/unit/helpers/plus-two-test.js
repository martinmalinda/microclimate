import {
  plusTwo
} from '../../../helpers/plus-two';
import { module, test } from 'qunit';

module('PlusTwoHelper');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = plusTwo(42);
  assert.ok(result);
});
