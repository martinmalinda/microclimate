export default function(){
	this.transition(
	  this.fromRoute('session.index'),
	  this.toRoute('session.detail'),
	  this.use('toLeft'),
	  this.reverse('toRight')
	);
}