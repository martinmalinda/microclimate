import Ember from 'ember';

export default Ember.Route.extend({
	model: function(params){
		return this.store.find('session', {orderBy: 'place', startAt:params.place_name, endAt:params.place_name});
	},
	afterModel(model){
		console.log(model);
	}
});
