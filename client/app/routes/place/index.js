import Ember from 'ember';

export default Ember.Route.extend({
	model: function(){
		return ['Zoologická posluchárna','Braunerova posluchárna','Braunerova poslucharna 2','Praktikum OŽP','Posluchárna OŽP','Krajinova poslucharna','Krajinova poslucharna 2','Seminarium BB','Kostirova poslucharna'];
	}
});
