import Ember from 'ember';

export default Ember.Route.extend({
	model(){
		return this.store.findAll('session');
	},
  afterModel(){
    return this.store.findAll('event');
  }
});
