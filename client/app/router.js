import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('user', function() {});
  this.route('session',{path: '/'}, function() {
    this.route('detail', {path: '/session/:session_id'}, function() {
      this.route('export');
    });
    this.route('detail2',{path: '/session2/:session_id'});
  });
  this.route('value', function() {});
  this.route('event', function() {});
  // this.resource('sessiondetail', {path: '/session/:session_id'}, function() {});
  this.route('place', function() {
    this.route('detail',{path: '/place/:place_name'});
  });
  this.route('rhcalculate');
  this.route('scatter');
  this.route('custom');
  this.route('edit');
});

export default Router;
