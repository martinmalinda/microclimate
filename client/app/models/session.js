import DS from 'ember-data';
// import { moment, ago } from 'ember-moment/computeds';
// import { moment, ago } from 'ember-moment/computeds';

const {computed, isPresent} = Ember;

export default DS.Model.extend({
  date: DS.attr('string'),
  place: DS.attr('string'),
  weather: DS.attr('string'),
  people: DS.attr('number'),
  lightMedian: DS.attr('number'),
  tempMedian: DS.attr('number'),
  rhMedian: DS.attr('number'),
  co2Median: DS.attr('number'),
  startTime: DS.attr('string'),
  endTime: DS.attr('string'),

  rhDiffCache: DS.attr({defaultValue: ""}),
  tempDiffCache: DS.attr({defaultValue: ""}),
  tempExtCache: DS.attr({defaultValue: ""}),
  rhExtCache: DS.attr({defaultVaue: ""}),
  co2DiffCache: DS.attr({defaultVaue: ""}),
  tempExtWholeDay: DS.attr('number'),
  rhExtWholeDay: DS.attr('number'),

  tempExt: computed.alias('tempExtCache'),

  co2Diff: computed('co2Values.[]',function(){
    return this.get('co2Values.lastObject.value') - this.get('co2Values.firstObject.value');
  }),

  //Relationships,

  events: DS.hasMany('event', {async: true}),
  values: DS.hasMany('value', {async: true}),

  monthNumber: computed('date', function(){
    var date = new Date(this.get('date'));
    return date.getMonth()+1;
  }),

  dayNumber: computed('date', function(){
    var sessionDate = new Date(this.get('date'));
    var start = new Date(sessionDate.getFullYear(), 0, 0);
    var diff = sessionDate - start;
    var oneDay = 1000 * 60 * 60 * 24;
    var dayNumber = Math.floor(diff / oneDay);
    return dayNumber;
    console.log(dayNumber);
  }),

  minuteOfDay: computed('startTime', function(){
    const time = moment(this.get('startTime'), "HH:mm");
    return time.get('hour') * 60 + time.get('minute');
  }),

  findInEvents(cb){
    return this.get('events').find(event => {
      if(event.get('content')){
        return cb(event);
      }
    }) ? true : false;
  },

  lightsOn: computed('events.[]', function(){
    return this.findInEvents(event => {
      const content = event.get('content').toLowerCase();
      return content.indexOf('světla') > -1 || content.indexOf('svetla') > -1;
    });
  }),

  windowsOpened: computed('events.[]', function(){
    return this.findInEvents(event => {
      const content = event.get('content').toLowerCase();
      return content.indexOf('otev') > -1 && content.indexOf('ok') > -1;
    });
  }),

  doorsOpened: computed('event.[]', function(){
    return this.findInEvents(event => {
      const content = event.get('content').toLowerCase();
      return content.indexOf('otev') > -1 && content.indexOf('dve') > -1;
    });
  }),

  co2IncreasePerPerson: computed.alias('peopleCo2Factor'),
  peopleCo2Factor: computed('co2Median', 'people', function(){
    const ambient = 400;
    if(isPresent(this.get('co2Median'))){
      return Math.round((Number(this.get('co2Median')) - 400)/Number(this.get('people')));
    }
  }),

  // tempExt: computed('values.[]', function(){
  //   return this.get('value').
  // })
  tempExtValues: computed.filterBy('values', 'type', 'tempExt'),
  rhExtValues: computed.filterBy('values', 'type', 'rhExt'),
  co2Values: computed.filterBy('values', 'type', 'co2'),

  tempDiff: computed('tempExtValues', function(){
    if(isPresent(this.get('tempMedian'))){
      return this.get('tempMedian') - this.get('tempExtWholeDay');
    }
  }),

  rhDiff: computed('rhExtValues', function(){
    if(isPresent(this.get('rhMedian'))){
      return this.get('rhMedian') - this.get('rhExtCache');
    }
  }),

  tempExtValue: computed.alias('tempExtValues.lastObject.value'),
  rhExtValue: computed.alias('rhExtValues.lastObject.value'),

  wundergroundDate: computed('date', function(){
    return moment(this.get('date')).format("YYYY/MM/DD");
  })
});
