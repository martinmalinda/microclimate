import DS from 'ember-data';

export default DS.Model.extend({
  session: DS.belongsTo('session'),
  time: DS.attr(),
  value: DS.attr('number'),
  type: DS.attr('string'),
  // x: DS.attr(),
  // y: DS.attr(),
  // cssY: DS.attr(),
  // cssX: DS.attr(),

  unit: function(){
    var type = this.get('type');
    if(type === 'temp' || type === 'tempExt'){
      return '°C';
    } else if (type === 'rh' || type === 'rhExt') {
      return '%';
    } else if (type === 'light') {
      return 'lux';
    } else if (type === 'co2'){
      return 'ppm';
    }
  }.property('type')
  //OBSELETE - CHART component should handle this

  // x: function(){
  //   // return parseInt(this.get('time'))*1.1+'%';
  // 	return parseInt(this.get('time'))*1.1;
  // }.property('time'),

  // y: function(){
  //   // return 100-parseInt(this.get('value'))/200*100+'%';
  // 	return 100-parseInt(this.get('value'))/200*100;
  // }.property('value'),

  // cssX: function(){
  //   return this.get('x')-0.2+'%';
  // }.property('x'),

  // cssY: function(){
  //   return 100-parseInt(this.get('y'))-1.2+'%';
  // }.property('y')

});
