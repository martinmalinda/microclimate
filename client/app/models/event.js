import DS from 'ember-data';

export default DS.Model.extend({
  session: DS.belongsTo('session'),
  from: DS.attr(),
  to: DS.attr(),
  content: DS.attr('string')
});
