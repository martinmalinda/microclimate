import Ember from 'ember';

export function plusTwo(value) {
	return parseInt(value)*2+2.5;
}

export default Ember.HTMLBars.makeBoundHelper(plusTwo);
