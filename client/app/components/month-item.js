import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ['card','clickable'],
	classNameBindings: ['isCurrent:red'],
	action: 'setMonth',

	percent: function(){
		var maxAmountOfSessions = 4*4;
		console.log(this.get('data.sessionsInMonth')/maxAmountOfSessions*100);
		return Math.floor(this.get('data.sessionsInMonth')/maxAmountOfSessions*100);
	}.property('data.sessionsInMonth'),

	isCurrent: function(){
		return this.get('selectedMonth') === this.get('data.number');
	}.property('selectedMonth'),
  	click: function(){
  		console.log('clicking?');
  		this.sendAction('action', this.get('data.number'));
  	}
});