import Ember from 'ember';
import layout from '../templates/components/session-item';

export default Ember.Component.extend({
  layout: layout,
  classNames: ['item'],
  session: Ember.inject.service('session'),
  action: 'deleteSession',
  actions: {
  	delete: function(){
  		this.get('data').destroyRecord();
  		this.sendAction('action', this.get('data'));
  	}
  }
});
