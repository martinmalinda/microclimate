import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ['event-line'],
	attributeBindings: ['style'],
	style: function(){
		var index = this.get('index')*2+2;
		var from = this.get('data.from')/90*100;
		var width = this.get('data.to')/90*100 - from;
		return "top:"+index+'%;left:'+from+'%;width:'+width+'%';
	}.property('data.from','data.to','index'),

	actions: {
		delete: function(){
			this.get('data').destroyRecord();
		}
	}
});
