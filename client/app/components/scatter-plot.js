import Ember from 'ember';

const {on, computed, isPresent} = Ember;

export default Ember.Component.extend({

	classNames: ['scatter-plot'],

	computeX(model){
		var rawX = model.get(this.get('xProperty'));
		var minX = this.get('minX');
		var maxX = this.get('maxX');

		var x = (rawX + minX)/(maxX + minX) * 100;
		// model.set('x', x);
		return x;
	},

	computeY(model){
		var rawY = model.get(this.get('yProperty'));
		var minY = this.get('minY')
		var maxY = this.get('maxY')
		var y = (rawY + minY)/(maxY + minY) * 100;
		// model.set('y', y);
		return y;
	},

	sumBy(propertyName){
		return this.get('points').reduce((previousValue, item) => {
			return item.get(propertyName);
		}, 0);
	},

	points: computed('model.[]','minY','maxY','minX','maxX','yProperty','xProperty', function(){

		var model = this.get('model');
		var points = [];
		// var point = {};

		var point = Ember.Object.extend({
			xy: computed('x', 'y', function(){
				return this.get('x')*this.get('y');
			})
		});

		model.forEach(model => {

			if(isPresent(model.get(this.get('xProperty'))) && isPresent(model.get(this.get('yProperty')))){
				points.push(point.create({
					model: model,
					x: this.computeX(model),
					y: this.computeY(model)
				}));
			}
			// this.computeX(model);
			// this.computeY(model);
		});

		return points;
	}),

	sumX: computed('points.[]', function(){
		return this.sumBy('x');
	}),

	sumY: computed('points.[]', function(){
		return this.sumBy('y');
	}),

	trendlineA: computed('points.[]', function(){
		var points = this.get('points');
		var sumXY = this.sumBy('xy');
		var sumX = this.get('sumX');
		var sumY = this.get('sumY');

		// var sqSumX = sumX*sumX;
		var sumXSq = points.reduce((previousItem, item) => {
			return item.get('x') * item.get('x');
		}, 0);

		var n = points.get('length');

		var alpha = (n*sumXY - sumX*sumY)/(n*sumXSq - sumX*sumX);
		return alpha;
	}),



	trendlineB: computed('trendlineA', 'points.[]', function(){
		var sumX = this.get('sumX');
		var sumY = this.get('sumY');
		var n = this.get('points.length');
		var alpha = this.get('trendlineA');
		console.log(`${sumY} - ${alpha}*${sumX}/${n}`);
		var beta = (sumY - alpha*sumX)/n;

		return beta;
	}),

	trendlinePoints: computed('trendlineA', 'trendlineB', 'points.[]',function(){
		var alpha = this.get('trendlineA');
		var beta = this.get('trendlineB');
		console.log('beta', beta);

		var trendline = function(x){
			console.log(`${x}*${alpha} + ${beta}`);
			return x*alpha + beta;
		}

		var trendlinePoints = {
			x1: 20,
			x2: 60,
		};

		trendlinePoints.y1 = 100 - trendline(trendlinePoints.x1);
		trendlinePoints.y2 = 100 - trendline(trendlinePoints.x2);

		return trendlinePoints;

	})
});
