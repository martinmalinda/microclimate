import Ember from 'ember';
import layout from '../templates/components/light-chart';

const {computed} = Ember;

export default Ember.Component.extend({
  layout: layout,
  classNames: ['light-chart'],
  model: 'data',
  // sortProperties:['x'],
  valuesSorting: ['x'],
  sortedPoints: Ember.computed.sort('primaryPoints','valuesSorting'),
  sortedExternalValues: Ember.computed.sort('externalPoints', 'valuesSorting'),
  action: 'saveMedian',
  sortAscending: true,
  data: null,

  session: Ember.inject.service('session'),

  xProperty: null,
  yProperty: computed(function(){}),

  showSession: false,

  printValues: false,

  backgroundStyle: Ember.computed('color', function(){
    // return new Ember.Handlebars.SafeString('background: '+ this.get('color'));
    return new Ember.Handlebars.SafeString('border:2px solid '+ this.get('color'));
  }),

    setCoordinates(values){
      var yProperty = this.get('yProperty');
      var xProperty = this.get('xProperty');
      var xMax = this.get('xMax');
      var xMin = this.get('xMin');
      var yMax = this.get('yMax');
      var yMin = this.get('yMin');
      // var self = this;

      // return Ember.run.scheduleOnce('afterRender', function(){
      if(values){
        values.forEach(function(value){
        var rawX = value.get(xProperty);
        var rawY = value.get(yProperty);
       
        if(typeof rawY === 'number'){
          var x = Math.round(rawX/(xMax + xMin)*100)+1;
          // console.log('rawX: '+rawX+' xMax: '+xMax+' xMin: '+ xMin);
          // console.log(x);
          value.set('x', x);
          var y = 100-((rawY)/(yMax - yMin)*100) + yMin/yMax*100;

          if(yMin < 0 && yMax > 0){
            y = 100 - (rawY - Math.abs(yMin))/(yMax + Math.abs(yMin))*100 + yMin/yMax*100;
            // 100 - (-100 - Math.abs(-100))/(100 + Math.abs(-100))*100 + -100/100*100
          }

          value.set('y',y);
          value.set('yLabel',rawY);
          var cssX = (Math.round(x-0.2)*100)/100+'%';
          // console.log(cssX);
          value.set('cssX',cssX);
          // console.log(value.get('cssX'));
          var cssY = 100-Math.round(parseInt(y)*100)/100-1.2+'%';
          value.set('cssY',cssY);
          value.set('style', new Ember.Handlebars.SafeString('left:'+cssX+';bottom:'+cssY));
          } else {
            value.set('x', false); // disable it in PolyLine
             value.set('style', new Ember.Handlebars.SafeString('display:none')); // disable it in CSS
             console.log('fail?');
          }
          // console.log(value.get('cssX'));
        });
        return values;
      }
    },

    primaryPoints: Ember.computed('content.[]', 'yProperty', function(){

      var primaryPoints = this.get('content');
        return this.setCoordinates(primaryPoints);
        
      // });

    }),
    externalPoints: Ember.computed('externalData.[]','yProperty', function(){
      var externalData = this.get('externalData');
      return this.setCoordinates(externalData);
    }),



  yMiddle: function(){
  	return (this.get('yMax')-this.get('yMin'))/2+this.get('yMin');
  }.property('yMin','yMax'),

  xMiddle: function(){
  	return (this.get('xMax')-this.get('xMin'))/2;
  }.property('xMin','xMax'),

  median: Ember.computed('content.[]','yMax','yMin', function(){
    var values = this.get('content');
    var yProp = this.get('yProperty');
    if(values && values.get('length') > 0){
      values = values.sortBy(yProp);
      var length = this.get('content.length');
      var half = Math.floor(length/2);
      if(length % 2){
        return values.objectAt(half).get(yProp);
      } else {
        return (values.objectAt(half-1).get(yProp) + values.objectAt(half).get(yProp))/2;
      }
    }

  }),

  medianY: function(){
    var yMax = this.get('yMax');
    var yMin = this.get('yMin');
    var y = 100-((this.get('median'))/(yMax - yMin)*100) + yMin/yMax*100;
    // this.sendAction('action',y);
    return y;
  }.property('median','yMax','yMin'),

  dataObserver: function(){
  	
  }.observes('data').on('init'),

  polyLinePoints: Ember.computed('sortedPoints.[]','yProperty','printValues', function(){

      var data = this.get('sortedPoints');
      console.log(data);
      var polyLinePoints = "";
      if(data){
      data.forEach(function(item){
        var x = item.get('x');
        var y = item.get('y');
        if(x){
          polyLinePoints += x+','+y+' ';
        } else {
        }
      });
    }
    return polyLinePoints;
  }),

  extPolyLinePoints: function(){

  var data = this.get('sortedExternalValues');
  console.log('extPolyLinePoints');
  console.log(data);

  	var polyLinePoints = "";
  	if(data){
//	  	var self = this;
		data.forEach(function(item){
      console.log(item);
			if(item.get('x')){
				polyLinePoints += item.get('x')+','+item.get('y')+' ';
			} else {
				item.get('x');
			}
		});
	}
	return polyLinePoints;
  }.property('sortedExternalValues.[]','yProperty'),

  actions: {
  	delete: function(value){
  		value.destroyRecord();
  		this.get('content').removeObject(value);
  		this.notifyPropertyChange('content');

  	},

    exportMedian: function(){
      this.sendAction('action',this.get('median'));
    }
  }
});
