import Ember from 'ember';
import layout from '../templates/components/value-item';

export default Ember.Component.extend({
  layout: layout,
  classNames: ['item'],
  session: Ember.inject.service('session'),
  actions: {
  	delete: function(){
  		this.get('data').destroyRecord();
  	}
  },
  click: function(){
  	console.log(this.get('data'));
  }
});
