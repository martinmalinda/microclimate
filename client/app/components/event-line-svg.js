import Ember from 'ember';

export default Ember.Component.extend({
	tagName: 'polyline',
	attributeBindings: ['points','stroke','stroke-width','style','strokeWidth:stroke-width','markerStart:marker-start','markerEnd:marker-end'],
	points: function(){
		//0,1 0,1 format
		var index = this.get('index')*2+2; //*2 for some more space
		return this.get('data.from')+','+index+' '+this.get('data.to')+','+index;
	}.property(),
	stroke: '#000',
	strokeWidth: '0.5',
	style: 'fill:none;',
	markerStart: 'url(#circle)',
	markerEnd: 'url(#circle)'
});
