import Ember from 'ember';

export default Ember.Service.extend({

  download(attrs, data, filename){
    let output = this.format(attrs, data);

    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(output));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);

  },

  format(attrs, data){
    let firstRow = this._formatFirstRow(attrs);
    return data.reduce((prev, item, index) => {
      console.log(item);
      let row = this._formatRow(attrs, item);
      if(index === 1) {
        let secondRow;
        if(typeof prev !== 'string'){
          secondRow = this._formatRow(attrs, prev);
        } else {
          secondRow = prev;
        }
        prev = firstRow + '\n' + prev;
      }
        return prev + row;
    });
  },

  _formatFirstRow(attrs){
    return attrs.reduce((prev, item) => {
        return prev + ',' + item;
    });
  },

  _formatRow(attrs, item){
    console.log(item);
    let row = "";
    return attrs.reduce((prev, attr, index) => {
      if(index === 1){
        prev = item.get(prev);
      }
      return prev + ',' + item.get(attr) + (this._isLastColumn(attrs, index) ? '\n' : '');
    });
  },

  _isLastColumn(attrs, n){
    return attrs.length === n + 1;
  }
});
