import Ember from 'ember';

const {
  computed,
  getWithDefault
} = Ember;

export default function cleanBy(collectionKey, mapProperty) {
  return computed(`${collectionKey}.[]`, mapProperty,'pickedPlaces.@each.toggled', function(){
    const yProp = this.get('chartPresets')[this.get(mapProperty)].propertyName;
    return this.get(collectionKey).rejectBy(yProp, "").sortBy(yProp).mapBy(yProp).compact();
  });
}