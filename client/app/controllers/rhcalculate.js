import Ember from 'ember';

const {computed} = Ember;

export default Ember.Controller.extend({
	temperature: 0,
	relativeHumidity: 100,

	satVapourPressure: computed('temperature', function(){
		var temp = Number(this.get('temperature'));
		console.log(temp);
		console.log(1/10);
		console.log(0.6108^((17.27*temp)/(temp + 237.3)));
		return 0.6108^((17.27*temp)/(temp + 237.3));
	}),

	satVapourPressure2: computed('temperature', function(){
		var t = this.get('temperature');

		var table = new Array(
			3.956,4.258,
			4.579,4.926,5.294,5.685,6.101,6.543,7.013,7.513,8.045,8.609,
			9.209,9.844,10.518,11.231,11.987,12.788,13.634,14.530,15.477,16.477,
			17.535,18.650,19.827,21.068,22.377,23.756,25.209,26.739,28.349,30.043,
			31.824,33.695,35.663,37.729,39.898,42.175,44.563,47.067,49.692,52.442,
			55.324,58.34,61.50,64.80,68.26,71.88,75.65,79.60,83.71,88.02,
			92.51,97.20,102.09, 107.20, 112.51,118.04,123.80,129.82,136.08,142.60,
			149.38,156.43,163.77,171.38,179.31,187.54,196.09,204.96,214.17,223.73,
			233.7,243.9,254.6,265.7,277.2,289.1,301.4,314.1,327.3,341.0,
			355.1,369.7,384.9,400.6,416.8,433.6,450.9,468.7,487.1,506.1,
			525.76,546.05,566.99,588.60,610.90,633.90,657.62,682.07,707.27,733.24,
			760.00,
			787.57,815.86
			);
	   var i = Math.floor(t);
	   var p = t - i;
	   var p2m1 = p * p - 1.0;
	   var p2m4 = p2m1 - 3.0;
	   i += 2;
	      
	   var torPressure = p2m1*p*(p-2)*table[i-2]/24.0 - (p-1)*p*p2m4*table[i-1]/6.0 + p2m1*p2m4*table[i]/4.0 - (p+1)*p*p2m4*table[i+1]/6.0 + p2m1*p*(p+2)*table[i+2]/24.0;
		return torPressure*0.13332; //pascals
	}),

	// absoluteHumidity: computed('satVapourPressure2','temperature', function(){
	// 	return (2165 * this.get('satVapourPressure2')) / (this.get('temperature') + 273.16) * 10;
	// })
	
	absoluteHumidity: computed('relativeHumidity','temperature', function(){
		var rh = Number(this.get('relativeHumidity'));
		var T = Number(this.get('temperature'));
		console.log(`(6.112 * Math.exp((17.67 * ${T})/(${T}+243.5)) * ${rh} * 2.1674)/(273.15 + ${T})`);
		console.log(`(6.112 * Math.E^((17.67 * ${T})/(${T}+243.5)) * ${rh} * 2.1674)/(273.15 + ${T})`);
		console.log(Math.pow(Math.E, 1));
		return (6.112 * Math.exp((17.67 * T)/(T+243.5)) * rh * 2.1674)/(273.15 + T);
		// return (6.112 * Math.pow(2.73, (17.67 * T)/(T+243.5)) * rh * 2.1674)/(273.15 + T);
		// return (6.112 * Math.pow((17.67 * T)/(T+243.5)) * rh * 2.1674)/(273.15 + T);
	})
});
