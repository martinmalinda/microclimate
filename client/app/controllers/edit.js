import Ember from 'ember';

export default Ember.Controller.extend({

  setIfUndefined(obj, property, value){
    if(typeof obj.get(property) === 'undefined'){
      obj.set(property, value);
    }
  },

  actions: {
    save(session){
      const simpleModel = session.toJSON();
      for (let attribute in simpleModel){
        this.setIfUndefined(session, attribute, "");
      }
      session.save();
    }
  }
});
