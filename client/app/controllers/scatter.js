import Ember from 'ember';
import cleanBy from '../macros/cleanBy';

const {inject, computed, inject} = Ember;

export default Ember.Controller.extend({

	csv: inject.service(),
	places: ['Zoologická posluchárna','Braunerova posluchárna','Braunerova poslucharna 2','Praktikum OŽP','Posluchárna OŽP','Krajinova poslucharna','Krajinova poslucharna 2', 'Seminarium BB','Kostirova poslucharna'],
	weathers: [{name: 'Zataženo', toggled: true}, {name: 'Polojasno', toggled: true}, {name: 'Slunečno', toggled: true}, {name: 'Ostatni', toggled: true}],

	pickedPlaces: [
	{
		name: 'Zoologická posluchárna',
		toggled: true
	},
	{
	name: 'Braunerova posluchárna',
	toggled: true
	},
	{
	name: 'Braunerova poslucharna 2',
	toggled: true
	},
	{
		name: 'Praktikum OŽP',
		toggled: true
	},
	{
	name: 'Posluchárna OŽP',
	toggled: true
	},
	{
	name: 'Krajinova poslucharna',
	toggled: true
	},
	{
	name: 'Krajinova poslucharna 2',
	toggled: true
	},
	{
		name: 'Seminarium BB',
		toggled: true
	},
	{
	name: 'Kostirova poslucharna',
	toggled: true
	}],

	lightsOn: true,
	lightsOff: true,

	windowsOpened: true,
	windowsClosed: true,

	doorsOpened: true,
	doorsClosed: true,

	xProperty: 'date',
	yProperty: 'rhMedian',
	options: ['date','lightMedian','tempKbely','rhKbely', 'tempMedian', 'rhMedian', 'co2Median', 'people', 'tempDiff', 'rhDiff', 'co2IncreasePerPerson', 'tempExt', 'rhExt','co2Diff','time'],

	chartPresets: {
		date: {
			min: 0,
			max: 364,
			propertyName: 'dayNumber'
		},
		people: {
			min: 0,
			max: 80,
			color: '#00C4BC',
			propertyName: 'people'
		},
		lightMedian: {
			min: 0,
			max: 500,
			color: '#F2C61F',
			propertyName: 'lightMedian'
		},
		tempMedian: {
			min: 0,
			max: 30,
			color: '#DC6868',
			propertyName: 'tempMedian'
		},
		rhMedian: {
			min: 0,
			max: 70,
			color: '#458AC6',
			propertyName: 'rhMedian'
		},
		co2Median: {
			min: 400,
			max: 2000,
			color: '#e07b53',
			propertyName: 'co2Median'
		},
		tempExt: {
			min: 0,
			max: 30,
			color: 'red',
			propertyName: 'tempExtCache'
		},
		tempKbely: {
			min: 0,
			max: 30,
			color: 'red',
			propertyName: 'tempExtWholeDay'
		},
		rhKbely: {
			min: 0,
			max: 100,
			color: 'blue',
			propertyName: 'rhExtWholeDay'
		},
		rhExt: {
			min: 0,
			max: 100,
			color: 'blue',
			propertyName: 'rhExtCache'
		},
		rhDiff: {
			min: -100,
			max: 0,
			color: 'blue',
			propertyName: 'rhDiffCache'
		},
		tempDiff: {
			min: 0,
			max: 25,
			color: 'red',
			propertyName: 'tempDiffCache'
		},
		co2IncreasePerPerson: {
			min: 0,
			max: 150,
			color: 'purple',
			propertyName: 'peopleCo2Factor'
		},
		co2Diff: {
			min: 0,
			max: 600,
			color: 'black',
			propertyName: 'co2DiffCache'
		},
		time: {
			min: 0,
			max: 1440,
			color: 'red',
			propertyName: 'minuteOfDay'
		}
	},

	filteredModels: computed('model.[]','pickedPlaces.@each.toggled','weathers.@each.toggled','lightsOn','lightsOff','windowsClosed','windowsOpened','doorsOpened', 'doorsClosed', function(model){
		let models = this.get('model');
		return models.filter(model => {
			let pickedPlace = this.get('pickedPlaces').findBy('name', model.get('place')).toggled;
			let pickedWeather = this.get('weathers').findBy('name', model.get('weather'));
			if(!pickedWeather) {
				pickedWeather = this.get('weathers').findBy('name', 'Ostatni');
			}
			pickedWeather = pickedWeather.toggled;

			let lightSettingMatches = (model.get('lightsOn') && this.get('lightsOn')) || (!model.get('lightsOn') && this.get('lightsOff'));
			let windowSettingMatches = (model.get('windowsOpened') && this.get('windowsOpened')) || (!model.get('windowsOpened') && this.get('windowsClosed'));
			let doorSettingMatches = (model.get('doorsOpened') && this.get('doorsOpened')) || (!model.get('doorsOpened') && this.get('doorsClosed'));
			return pickedPlace && lightSettingMatches && windowSettingMatches && doorSettingMatches && pickedWeather;
		});
	}),

	modelSorting: computed('yProperty', function(){
		return [this.get('yProperty')];
	}),
	sortedModels: computed.sort('filteredModels', 'modelSorting'),
	xMedian: computed('cleanData.[]','yProperty', function(){
		return this.computeMedian('cleanData');

	}),

	thirdQuartile: computed('cleanData.[]', function(){
		return this.computeMedian('cleanData', 1.33333);
	}),

	firstQuartile: computed('cleanData.[]', function(){
		return this.computeMedian('cleanData', 4);
	}),

	iqr: computed('thirdQuartile', 'firstQuartile', function(){
		return this.get('thirdQuartile') - this.get('firstQuartile');
	}),

	cleanData: cleanBy('sortedModels', 'yProperty'),

	computeMedian(dataProp, n = 2){

		const values = this.get(dataProp);
		const half = values.get('length')/n;
		if(values.get('length') > 0){
			if(half % 2){
				const upper = Math.ceil(half);
				const lower = Math.floor(half);
				return ((values[upper] + values[lower])/2).toFixed(2);
			} else {
				return values[half].toFixed(2);
			}
		}
	},

	summerData: computed.filter('sortedModels', function(session){
		return session.get('dayNumber') < 269;
	}),

	cleanSummerData: cleanBy('summerData', 'yProperty'),
	summerMin: computed.min('cleanSummerData'),
	summerMax: computed.max('cleanSummerData'),

	summerMedian: computed('summerData','yProperty', function(){
		return this.computeMedian('cleanSummerData');
	}),

	cleanWinterData: cleanBy('winterData', 'yProperty'),
	winterMin: computed.min('cleanWinterData'),
	winterMax: computed.max('cleanWinterData'),

	max: computed.max('cleanData'),
	min: computed.min('cleanData'),

	winterData: computed.filter('sortedModels', function(session){
		return session.get('dayNumber') > 270;
	}),

	winterMedian: computed('winterData','yProperty', function(){
		return this.computeMedian('cleanWinterData');
	}),

	xSettings: computed('xProperty', function(){
		return this.get('chartPresets')[this.get('xProperty')];
	}),

	ySettings: computed('yProperty', function(){
		return this.get('chartPresets')[this.get('yProperty')];
	}),

	actions: {
		export() {
			const props = this.getProperties('xProperty', 'yProperty');
			const chartPresets = this.get('chartPresets');
			const attrs = ['date', 'people','weather','place', chartPresets[props.xProperty].propertyName, chartPresets[props.yProperty].propertyName];

			this.get('csv').download(attrs, this.get('sortedModels'), 'scatter' + this.get('xProperty') + '/' +  this.get('yProperty') + '.csv');
		},
		togglePlace(place){

		}
	}
});
