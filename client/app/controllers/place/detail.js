import Ember from 'ember';

const {computed, RSVP, inject} = Ember;

export default Ember.Controller.extend({
	queryParams: ['chartType'],

	csv: inject.service(),

	monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen','Červenec','Spren','Září','Říjen','Listopad','Prosinec'],

	placeName: function(){
		return this.get('model.firstObject').get('place');
	}.property('model'),
	chartPresets: {
		people: {
			min: 0,
			max: 80,
			color: '#00C4BC',
			yProperty: 'people'
		},
		light: {
			min: 0,
			max: 500,
			color: '#F2C61F',
			yProperty: 'lightMedian'
		},
		temp: {
			min: 0,
			max: 40,
			color: '#DC6868',
			yProperty: 'tempMedian'
		},
		rh: {
			min: 0,
			max: 100,
			color: '#458AC6',
			yProperty: 'rhMedian'
		},
		co2: {
			min: 400,
			max: 2000,
			color: '#e07b53',
			yProperty: 'co2Median'
		},
		tempExt: {
			min: -20,
			max: 40,
			color: 'red',
			yProperty: 'tempExtCache'
		},
		rhExt: {
			min: 0,
			max: 100,
			color: 'blue',
			yProperty: 'rhExtCache'
		},
		rhDiff: {
			min: -100,
			max: 100,
			color: 'blue',
			yProperty: 'rhDiffCache'
		},
		tempDiff: {
			min: 0,
			max: 20,
			color: 'red',
			yProperty: 'tempDiffCache'
		},
		co2PerPerson: {
			min: 0,
			max: 150,
			color: 'purple',
			yProperty: 'peopleCo2Factor'
		}
	},
	chartSettings: function(){
		var settings = Ember.Object.create({});
		var chartType = this.get('chartType');
		return this.get('chartPresets.' + chartType);
	}.property('chartType'),

	chartType: 'people',

	allValuesLoaded: computed('model', function(){
		var promises = this.get('model').mapBy('values');

		RSVP.all(promises).then(() => {
			this.set('allValuesLoaded', true)
		});
	}),

	actions: {
		switchChartType: function(chartType){
			this.set('chartType',chartType);
		},
		export(){
			this.get('csv').download(
				['dayNumber','people','weather','lightMedian','tempMedian','rhMedian','co2Median','tempExtValue','rhExtValue'],
				this.get('model'),
				'place.csv');
		}
//		showSession: function(value){
			// console.log(value);
			// this.transitionToRoute('session.detail',value.get('session'));
		//}
	}

	// chartData: function(){
	// 	return this.get('model');
	// }.property('chartSettings.@each','model.@each')
});
