import Ember from 'ember';

const {isPresent} = Ember;

export default Ember.Controller.extend({
	saveCount: 0,

	actions: {
		cacheValues(){
			this.get('model').forEach(session => {
				session.get('values').then(() => {

					var rhDiff = session.get('rhDiff');
					var tempDiff = session.get('tempDiff');
					var rhExtValue = session.get('rhExtValue');
					var tempExtValue = session.get('tempExtValue');

					if (isPresent(rhDiff)){
						session.set('rhDiffCache', rhDiff);
					}
					if (isPresent(tempDiff)){
						session.set('tempDiffCache', tempDiff);
					}
					if (isPresent(rhExtValue)){
						session.set('rhExtCache', rhExtValue);
					}
					if (isPresent(tempExtValue)){
						session.set('tempExtCache', tempExtValue);
					}
					
					session.save().then(() => {
						this.incrementProperty('saveCount');
						console.count();
					});
				});
			});
		}
	}
});
