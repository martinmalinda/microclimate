import Ember from 'ember';

const {inject} = Ember;

export default Ember.Controller.extend({
	queryParams: ['chartType'],
	csv: 'time,type,value',
	values: null,

	// new Event Params
	from: 0,
	to: 0,
	content: null,

	chartType: 'light',
	session: inject.service(),
	csvService: inject.service('csv'),

	startMinute: function(){
		var startTime = this.get('model.startTime').split(':');
		var hour = parseInt(startTime[0]);
		var minute = parseInt(startTime[1]);
		return hour*60+minute;
	}.property('startTime'),

	//Should be in one chartSettings object
	chartMax: function(){
		var chartType = this.get('chartType');
		if(chartType === 'light'){
			return 500;
		} else if(chartType === 'temp') {
			return 30;
		} else if(chartType === 'rh'){
			return 100;
		} else if(chartType === 'co2'){
			return 3000;
		} else if(chartType === 'comfort'){
			return 12;
		}

	}.property('chartType'),
	chartMin: function(){
		var chartType = this.get('chartType');
		if(chartType === 'light'){
			return 0;
		} else if(chartType === 'temp') {
			return 0;
		} else if(chartType === 'rh'){
			return 0;
		} else if(chartType === 'co2'){
			return 400;
		} else if(chartType === 'comfort'){
			return 0;
		}

	}.property('chartType'),
	chartColor: function(){
		var chartType = this.get('chartType');
		console.log('chartColor');
		if(chartType === 'light'){
			return '#f2c61f';
		} else if(chartType === 'temp') {
			return '#d95c5c';
		} else if(chartType === 'rh'){
			return '#3b83c0';
		} else if(chartType === 'co2'){
			return '#E28560';
		} else if(chartType === 'comfort'){
			return '#00b5ad';
		}
	}.property('chartType'),

	chartValues: function(){
		var chartType = this.get('chartType');
		var values = this.get(chartType+'Values');
		console.log(chartType+'Values');
		console.log(values);
		// console.log(values);
		return values;
	}.property('chartType','model.values.[]','model.values.isFulfilled'),

	externalChartValues: function(){
		var chartType = this.get('chartType');
		var values = this.get(chartType+'ExtValues');
		console.log('extValues');
		console.log(values);
		return values;

	}.property('chartType'),

	weatherIcon: function(){
		var weather = this.get('model.weather');
		if(weather === 'Slunečno') {
			return 'sun';
		} else {
			return 'cloud';
		}
	}.property('model.weather'),

	lightValues: Ember.computed.filterBy('model.values','type','light'),
	tempValues: Ember.computed.filterBy('model.values','type','temp'),
	rhValues: Ember.computed.filterBy('model.values','type','rh'),
	co2Values: Ember.computed.filterBy('model.values','type','co2'),
	tempExtValues: Ember.computed.filterBy('model.values','type','tempExt'),
	rhExtValues: Ember.computed.filterBy('model.values','type','rhExt'),

	allValues: Ember.computed.union('lightValues', 'tempValues', 'rhValues', 'co2Values', 'tempExtValues', 'rhExtValues'),

	parsedCsv: function() {
		return Papa.parse(this.get('csv'),{header: true});
	}.property('csv'),

	actions: {
		import: function(){
			var session = this.get('model');
			// console.log(sessionId);
			var self = this;
			var records = this.get('parsedCsv').data;
			console.log(records);
			var requests = [];
			//TODO - use pushObjects() here obviously
			records.forEach(function(item){
				item.session = session;
				item.value = Number(item.value);
				var newValue = self.store.createRecord('value', item);
				requests.push(newValue.save());
				// newValue.save().then(function(){
				// 		// values.pushObject(newValue);
				// 	});
			});
			Ember.RSVP.all(requests).then(function(){
				session.save();
				console.log('values imported successfuly');
				//self.get('model').reload();
			});
		},

		updateSession: function(){
			this.get('model').save();
		},

		addEvent: function(){
			var data = this.getProperties('from','to','content');
			data.session = this.get('model');
			console.log(this.get('model'));

		//			var events = this.get('events');
		var newEvent = this.store.createRecord('event', data);

		var self = this;
		newEvent.save().then(function(){
			self.get('model').save();
		});
	},

	setChartValues: function(chartType){
		this.set('chartType', chartType);
	},

	saveMedian: function(median){
		var medianType = this.get('chartType')+'Median';
		var model = this.get('model');
		model.set(medianType,median);
		console.log('saving median');
		model.save();
	},

	dumpValues: function(){
		if(confirm('opravdu smazat?')){
			this.get('model.values').forEach(function(item){
				item.destroyRecord();
			});
		}
	},

	dumpCurrentTypeValues(){
		var type = this.get('chartType');
		var values = this.get(type + 'Values');
		if(confirm('opravdu smazat? Polozek:' + values.get('length'))){

			values.forEach(item => {
				item.destroyRecord();
			});
		}
	},

	importExternalValues: function(){
		var firstRecordNumber = Math.ceil(this.get('startMinute')/20); //35
		var date = new Date(this.get('model.date'));
		var dateF = date.getFullYear().toString() + ("0" + (date.getMonth() + 1)).slice(-2) + ("0" + date.getDate()).slice(-2);
		var url = 'http://api.wunderground.com/api/c1d03c8fd31a120a/history_' + dateF + '/q/CZ/Prague.json';

		var previousMin = null;
		var time = 0;
		var self = this;
		var sessionId = this.get('model.id');
		var model = this.get('model');

		var requests = [];

		Ember.$.getJSON(url, function(data){
			for(var i=firstRecordNumber;i <= firstRecordNumber+8;i++){
				var observation = data.history.observations[i];
				if(observation.date.min !== previousMin){ //check against duplicate values
					var rhData = {
						type: 'rhExt',
						value: Number(observation.hum),
						time: Number(time),
						session: model
					};

					var tempData = {
						type: 'tempExt',
						value: Number(observation.tempm),
						time: Number(time),
						session: model
					};


					requests.push(self.store.createRecord('value', rhData).save());
					requests.push(self.store.createRecord('value', tempData).save());
					time = time + 30;
					previousMin = observation.date.min;

				}
			}
			Ember.RSVP.all(requests).then(function(){
				model.save();
			});
		});
	},

	calculateComfort: function(){
		var tempValues = this.get('tempValues');
		var lightValues = this.get('lightValues');
		var rhValues = this.get('rhValues');
		var co2Values = this.get('co2Values');
		var sessionId = this.get('model.id');

		var self = this;

//		var comfortValues = [];
		if(co2Values){
			co2Values.forEach(function(item){
				let time = item.get('time');
				let co2 = item.get('value');
				let temp = tempValues.findBy('time',time).get('value');
				let rh = rhValues.findBy('time',time).get('value');
				let light = lightValues.findBy('time',time).get('value');
				
				let comfort = 12;
				if(co2 >= 2000) { comfort = comfort-3; } else
				if(co2 >= 1500) { comfort = comfort-2; } else
				if(co2 >= 800) { comfort = comfort-1; }

				if(temp >= 30 || temp <= 16) { comfort = comfort-3; } else
				if(temp >= 28 || temp <= 17) { comfort = comfort-2; } else
				if(temp >= 25 || temp <= 19) { comfort = comfort-1; }

				if(rh >= 80 || rh <= 30) { comfort = comfort-2; } else
				if(rh >= 70 || rh <= 40) { comfort = comfort-1; } else
				if(rh >= 20) { comfort = comfort-3; }

				if(light <= 10) { comfort = comfort-3; } else 
				if(light <= 30) { comfort = comfort-2; } else 
				if(light <= 100 || light >= 250) { comfort = comfort-1; }

				var comfortData = {
					time: time,
					value: comfort,
					type: 'comfort',
					session: sessionId

				};

				self.store.createRecord('value', comfortData).save();

			});
		}
	},
	export() {
		let attrs = Ember.A(['time','type', 'value']);
		this.get('csvService').download(attrs, this.get('allValues'), this.get('model.id') + '.csv');
	}
}
});
