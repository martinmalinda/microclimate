import Ember from 'ember';

export default Ember.Controller.extend({
	places: ['Zoologická posluchárna','Braunerova posluchárna','Braunerova poslucharna 2','Praktikum OŽP','Posluchárna OŽP','Krajinova poslucharna','Krajinova poslucharna 2', 'Seminarium BB','Kostirova poslucharna'],
	pickedPlaces: {
	"Zoologická posluchárna" : true,
	"Braunerova posluchárna" : true,
	"Braunerova poslucharna 2" : true,
	"Praktikum OŽP" : true,
	"Posluchárna OŽP" : true,
	"Krajinova poslucharna" : true,
	"Krajinova poslucharna 2" : true,
	"Seminarium BB" : true,
	"Kostirova poslucharna" : true
	},

	selectedPlace: null,
	selectedDate: null,
	selectedPeople: null,
	selectedWeather: null,
	session: Ember.inject.service('session'),

	selectedMonth: function(){
		var date = new Date();
		var monthNumber = date.getMonth();
		return this.get('months').objectAt(monthNumber);
	}.property('months'),

	months: Ember.computed('model.[]', function(){
		var sessions = this.get('model');
		var monthNames = ['Leden','Únor','Březen','Duben','Květen','Červen','Červenec','Spren','Září','Říjen','Listopad','Prosinec'];
		var months = [];
		// var month = Ember.Object.Extend({

		// });
		for(var i = 0;i<=11;i++){

			var sessionsInMonth = 0;
			var monthNumber = i+1;
			sessions.forEach(function(item){
				if(item.get('monthNumber') === monthNumber) {
					sessionsInMonth++;
				}


			});

			var newMonth = Ember.Object.create({
				name: monthNames[i],
				number: monthNumber,
				sessionsInMonth: sessionsInMonth
			});
			months.pushObject(newMonth);
		}
		return months;
	}),

	sessions: Ember.computed('model.[]','selectedMonth', function(){
		var selectedMonth = this.get('selectedMonth.number');
		return this.get('model').filterBy('monthNumber',selectedMonth).sortBy('date','startTime');
	}),

	actions: {
		newSession: function(){
			var data = {
				place: this.get('selectedPlace'),
				date: this.get('selectedDate'),
				people: this.get('selectedPeople'),
				weather: this.get('selectedWeather')
			};
			console.log('creating session');
			console.log(data);
			var record = this.store.createRecord('session',data);
			//var self = this;
			record.save().then(function(){
				
			});
		},

		deleteSession: function(session){
			console.log(session);
			this.get('model').removeObject(session);
			// this.notifyPropertyChange('sessions');
		},

		setMonth: function(number){
			var month = this.get('months').objectAt(number-1);
			this.set('selectedMonth', month);
		},

		transformJSON(){
			//var newJSON = {};
			//var self = this;
			
			// $.getJSON('./session.json', function(data){
			// 	console.log(data);
			// 	data.forEach(function(session, index){
			// 		console.log(session);
			// 		newJSON = {
			// 			date: session.date,
			// 			place: session.place,
			// 			people: session.people,
			// 			id: session._id.$oid,
			// 			co2Median: session.co2Median,
			// 			rhMedian: session.rhMedian,
			// 			lightMedian: session.lightMedian,
			// 			tempMedian: session.tempMedian,
			// 			weather: session.weather,
			// 			startTime: session.startTime,
			// 		};
			// 		self.store.createRecord('session', newJSON).save();

			// 	});
			// });
				var self = this;
			Ember.$.getJSON('./event.json', function(data){
				data.forEach(function(event, index){
						console.log(self.store);
						console.log(self.store);
						var session = self.store.peekRecord('session', event.session);
						if(session){
							var newEvent = self.store.createRecord('event', {
								from: event.from,
								content: event.content,
								to: event.to,
								session: session,
							});
							session.get('events').addObject(newEvent);
							console.log(newEvent);
							newEvent.save().then(function(){
								console.log('saved');
							});
						}
				});
				self.store.peekAll('session').forEach(function(session){
					session.save().then(function(session){
						console.log(session.get('id'));
					});
				});
			});
		}
	}
});