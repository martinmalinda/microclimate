import Ember from 'ember';

const {inject} = Ember;

export default Ember.Controller.extend({
	csv: 'test',
	values: null,

	// new Event Params
	from: 0,
	to: 0,
	content: null,

	csvService : inject.service('csv'),

	weatherIcon: function(){
		var weather = this.get('session.weather');
		if(weather === 'Slunečno') {
			return 'sun';
		} else {
			return 'cloud';
		}
	}.property('session.weather'),

	valuesObserver: function(){
		var values = this.get('values');
		var self = this;
		values.then(function(){
			let lightValues = values.filterBy('type','light');
			self.set('lightValues',lightValues);
			self.set('chartValues',lightValues);

			let tempValues = values.filterBy('type','temp');
			self.set('tempValues',tempValues);

			let rhValues = values.filterBy('type','rh');
			self.set('rhValues',rhValues);
		});

	}.observes('values'),

	lightValues: null,
	tempValues: null,

	parsedCsv: function() {
		return Papa.parse(this.get('csv'),{header: true});
	}.property('csv'),

	actions: {
		import: function(){
			var sessionId = this.get('session').get('id');
			console.log(sessionId);
			var self = this;
			var records = this.get('parsedCsv').data;
			console.log(records);
			records.forEach(function(item, index){
				item.session = sessionId;
				self.store.createRecord('value', item).save();
			});
			this.notifyPropertyChange('values');
		},

		updateSession: function(){
			this.get('session').save();
		},

		addEvent: function(){
			var data = this.getProperties('from','to','content');
			data.session = this.get('session').get('id');

			this.store.createRecord('event', data).save();
		},

		setChartValues: function(chartType){
			var chartValues = this.get(chartType+'Values');
			this.set('chartType', chartType);
			this.set('chartValues', chartValues);
		},

		export() {
			let attrs = ['type', 'value'];
			console.log(this.get('csv').download(attrs, this.get('chartValues')));
		}
	}
});