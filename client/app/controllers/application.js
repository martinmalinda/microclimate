import Ember from 'ember';

export default Ember.Controller.extend({
	showLoginForm: false,
	email: 'malindacz@gmail.com',
	password: '',
	session: Ember.inject.service('session'),
	actions: {
		toggleLoginForm(){
			this.toggleProperty('showLoginForm');
		},
		login(){
			this.get('session').authenticate('authenticator:firebase', {
                'email': this.get('email'),
                'password': this.get('password')
            }).then(function() {
                //this.transitionToRoute('index');
                this.set('showLoginForm', false);
            }.bind(this));
		}
	}
});
